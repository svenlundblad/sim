package sim;

abstract class GObject {
    /** masss in kg, if not yet set it should be Double.NaN */
    protected double mass = Double.NaN;

    protected Material mainMaterial = Material.steel;

    /**
     * Calculate the mass if it is not already set.
     * @return the calculated mass of this object.
     */
    protected abstract double calculateMass();

    public double getMass() {
        if(Double.isNaN(mass)) {
            /* Mass value was never set so calculate it. */
            mass = calculateMass();
        }
        return mass;
    }
}
