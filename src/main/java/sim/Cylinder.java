package sim;

public class Cylinder extends GObject {
    private double innerDiameter;
    private double outerDiameter;
    private double height;
    private double definingVolume = Double.NaN;

    public Cylinder(double outerDiameter, double innerDiameter, double height) {
        super();
        if(height < 0 || outerDiameter < 0) throw new IllegalArgumentException("Dimensions must be greaterthan 0");
        if(outerDiameter < innerDiameter) throw new IllegalArgumentException("The inner diameter is greater than the outer diameter of the cylinder");
        this.outerDiameter = outerDiameter;
        this.innerDiameter = innerDiameter;
        this.height = height;
    }

    private double getDefiningVolume() {
        if(Double.isNaN(definingVolume)) {
            definingVolume = ((Math.PI * outerDiameter * outerDiameter / 4) - (Math.PI * innerDiameter * innerDiameter / 4)) * height;
        }
        return definingVolume;
    }

    protected double calculateMass() {
        return mainMaterial.getDensity() * getDefiningVolume();
    }

    public static void main(String args[]) {
        System.out.println("Starship mass: " + new Cylinder(9.0, 9.0 - 0.008, 30).getMass());
    }
}
