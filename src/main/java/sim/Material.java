package sim;

abstract class Material {
    public static final Material steel = new Steel();

    /** kg/m3 */
    protected double density;

    public double getDensity() {
        return density;
    }
}

class Steel extends Material {
    
    Steel() {
        density = 7700.0;
    }
}
