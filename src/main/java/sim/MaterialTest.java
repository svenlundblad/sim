package sim;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MaterialTest {
	private Material m;
	
	@Before
	public void setUp() throws Exception {
		m = Material.steel;
	}

	@Test
	public void testDensity() {
		double MAX_DENSITY = 25 * 1000; // kg/m3 this should cover materials on earth
		assertTrue("Density is out of bounds", m.getDensity() > 0.0 && m.getDensity() < MAX_DENSITY);
	}

}
